---
layout: page
title: Bahn
permalink: /bahn/
nav_order: 60
---

## S-Bahn und Regionalverkehr
Der Schienenverkehr im Netz der Deutschen Bahn ist ideal das Rückgrat im ÖPNV neben der Straßenbahn und zentral für den Regionalverkehr und die Anbindung ländlicher Regionen an den Ballungsraum Augsburg.

Dafür sind jedoch noch viele Bausteine notwendig, unter anderem die Elektrifizierung von Bahnstrecken, der Ausbau von Bahnstrecken auf mehrere Gleise, die Wieder-/Neueröffnung von Haltestellen und eine Reaktivierung von Schienen.

**Stand Mai 2022: Reaktivierung Haltepunkt Hirblinger Straße und Neusäß Vogelsang weiter in Planung**

AZ zu Hirbliner Straße: [Link](https://www.augsburger-allgemeine.de/augsburg/augsburg-bahnausbau-haelt-der-zug-bald-in-der-hirblinger-strasse-id62490491.html)

AZ zu Vogelsang: [Link](https://www.augsburger-allgemeine.de/augsburg-land/neusaess-bahnausbau-ulm-augsburg-haelt-bald-ein-zug-in-vogelsang-id62485821.html)

### Wir fordern ... sofort:
Schnelle Aufwertung des ÖPNV durch:
- Nulltarif am Wochenende und an Feiertagen,
- 365€-Ticket für alle für den gesamten AVV-Verbundraum
- Freifahrschein für den gesamten AVV-Verbundraum für alle Senior\*innen, die ihren Führerschein abgeben.
- Verzicht auf Strafanzeigen für Schwarzfahren
- gekennzeichnete Mitfahr-Wartebereiche/-bänke am Stadtrand.
- Etablierung von Schnellbussen auf den Bundesstraßen mit enger Taktung im AVV-Gebiet
- Schaffung einer schnellen ÖPNV-Umfahrung von Augsburg
- Verknüpfung von DB-Bahnhöfen, Straßenbahnästen und Randzielen mittels Schnellbuslinien
- Einrichtung von Flexbussen, um Bahn und Schnellbuslinien ohne PKW erreichbar zu machen
- Einführung der S-Bahn im Taktverkehr: zwischen Meitingen und Bobingen (mit Elektrifizierung) und zwischen Neusäß (Reaktivierung drittes Gleis) zum Augsburger Hauptbahnhof nach Mering/Friedberg.

AVV muss drei Varianten für einen neuen Nahverkehrsplan entwickeln:
- 1) ÖPNV wie heute weiterbetreiben und aufrechterhalten trotz sinkender Einnahmen (Dies ist das, was der AVV heute als Aufgabe hat)
- 2) ÖPNV Nutzerzahlen verdoppeln bis 2026. Ausarbeitung eines "Nahverkehrsplanes x2" inklusive Ausarbeitung neuer Linien, Fahrzeugbedarf, Personalbedarf, Finanzierung,
- 3) ÖPNV Nutzerzahlen verdreifachen bis 2030.  Ausarbeitung eines "Nahverkehrsplanes x3" inklusive Ausarbeitung neuer Linien, Fahrzeugbedarf, Personalbedarf, Finanzierung.
- Mit diesem Nahverkehrsplan x2 und x3 und dem Finanzbedarf soll dann die Stadt/AVV beim Landtag / Bundestag um Fördermittel kämpfen. 
Beispiel [hier](https://www.verkehr4x0.de/wp-content/uploads/2021/10/211020_Buergerantrag_Nahverkehrsplan_V2.pdf) erklärt. 

Zudem Beschlüsse und Vorbereitung bzw. Planung für alle weiteren Maßnahmen, u.a. autofreie Zonen, Nulltarif und Ausbau der Tramlininen, Elektrifizierung der noch fehlenden Bahnstrecken und Reaktivierung und Neubau von Bahn Haltepunkten.
Alle Planungen für Neu- und Ausbau von Straßen werden gestoppt.


### 1-2 Jahre später sollte gelingen:
- Nulltarif in Bussen und Bahnen für Fahrgäste mit geringen Einkünften und als Prämie für den Verzicht auf ein privates Auto.
- Stärkung der Bahn mit weiteren Haltepunkten und Reaktivierung des dritten Gleises zwischen Neusäß und dem neuen Bahnhof Hirblinger Straße, um dichtere Taktzeiten zu erreichen (mehr Zugbegegnungen/-überholungen).
- Vollständige Elektrifizierung der Netze ins Allgäu
- ICE-Trasse nach München mit Weichen zu den Regionalverkehrsgleisen verbinden, um auf Störungen flexibel reagieren zu können
- Bau der dezentralen Mobilitätszentren (Schnellbuskreuz) mit Serviceangeboten (z. B. Gersthofen, Uni, Lechhausen AZ)
- Abschluss der Planungen und Machbarkeitsstudien für die neuen Tramlinien und den Ausbau des Schienennetzes der Deutschen Bahn 
- Weitere Haltestelle der S-Bahn/RB an der Fußballarena - wurde von der CSU schon 2008 gefordert ([Link zum AZ-Artikel](https://www.augsburger-allgemeine.de/augsburg/Plaene-fuer-neues-FCA-Stadion-Mit-der-Bahn-zur-impuls-arena-id3953381.html))
- Eröffnung der Bahn Haltepunkte Neusäß-Vogelsang (Biburg) und Friedberg-Paar (Harthausen)
- Reaktivierung der nun elektrifizierten Staudenbahn (Bahnstrecke Gessertshausen - Langenneufnach)
- Eröffnung des Güterverkehrszentrums (GVZ)


### In den Jahren danach wird alles fertig:
- Eröffnung aller neuer Tramlinien und ausgebauter Bahnlinien und Haltepunkte inklusive Taktverkehr
- Nulltarif für alle. Abschaffung aller Fahrkarteninfrastruktur und -bürokratie.
- Ausbau der Bahntrasse Oberhausen - Donauwörth (bzw. Meitingen) vierspurig (oder dreispurig) um einen engen Takt der S-Bahn zu ermöglichen ([Informationen zur S-Bahn Augsburg](https://de-academic.com/dic.nsf/dewiki/1214677#sel=))
- Trennung des Regional- und  Fernverkehrs zwischen Augsburg und Ulm auf eigenen zweispurigen Trassen
- Umsetzung des Regio-Schienen-Taktes (Zielnetz): enger Takt nach Gesserthausen, Friedberg, Mering, Meitingen und Bobingen, außerdem Taktverkehr nach Dinkelscherben, Fischach/Langenneufnach/Markt Wald, Donauwörth, Aichach und Buchloe
- Eröffnung der Güterzugumfahrungsstrecke rund um Augsburg


## Güterverkehr Schiene - Augsburger Localbahn
- **Sofortiger Bau des GVZ (Güterverkehrszentrums)** (4 Gleise, 2 Kräne, 700m Länge)
- Güterbahnhof bzw. -gleis in Graben
- Reaktivierung der stillgelegten Gleisanlagen der Augsburger Localbahn
- Ausbau der angefahrenen Gewerbeflächen und direkter Anschluss an das GVZ 
- GVZ und Umfahrungsstrecke verbinden in beide Richtungen
- Bau der Güterzugumfahrungsstrecke rund um Augsburg: Entlastuung von Gleisanlagen am Hauptbahnhof, weniger Lärmbelastung im Stadtbereich, mehr Kapazität am Hauptbahnhof für Personenverkehr durch Ein- und Ausfahrten und insgesamt mehr und stabilere Güterzuganbindung nach Augsburg



