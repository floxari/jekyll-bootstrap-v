---
layout: page
title: Kontakt
permalink: /kontakt/
nav_order: 110
---

### Presseanfragen
Bei allen Anliegen (inkl. Presseanfragen) zum Verkehrswendeplan Augsburg schreibt bitte an verkehrswende-augsburg@riseup.net

### Du willst mitmachen oder selbst aktiv werden?
Mit Aktionen, politischer Einmischung und auch euren Ideen und Vorschlägen wollen wir die Verkehrswende in und um Augsburg voranbringen. Startet eigene Aktionen in euren Straßen und Wohnvierteln. Wir helfen bei der Gründung weiterer Initiativen in Stadtteilen oder den Orten rund um Augsburg. Gerne kommen wir auch zu euch für Vorträge oder einen Austausch über die Vorschläge.
**Dann schreib eine Mail an [verkehrswende-augsburg@riseup.net](mailto:verkehrswende-augsburg@riseup.net).**

**Komm zu den nächsten Aktionen und spreche uns an (siehe Terminplan auf der Startseite)!**

### Website/Kontakt rechtlich verantwortliche Person: 
Ingo Blechschmidt, ladungsfähige Anschrift: Weber & Wir, Schussenstr. 1, 88212 Ravensburg, iblech@web.de, +49 176 95110311

