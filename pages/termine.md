---
layout: page
title: Termine
permalink: /termine/
nav_order: 15
---
# Termine

## **Nächste große Aktionen:**
    
    Coming Soon...


### **Jeden letzten Freitag im Monat um 18 Uhr am Rathausplatz: Critical Mass**
[https://criticalmass-augsburg.de/](https://criticalmass-augsburg.de/)
Eine Gruppe von Radfahrer\*innen fährt gemeinsam durch die Stadt – Wir sind der Verkehr!

### Weitere Aktionen sind in Planung :)

---
Der Standort der Raddemos wie auch der Critical Mass wird über CriticalMaps online geteilt.
Weitere Fragen zu den Veranstaltungen gerne an die Mailadresse unter Kontakt.


### Du willst mitmachen oder selbst aktiv werden?
Mit Aktionen, politischer Einmischung und auch euren Ideen und Vorschlägen wollen wir die Verkehrswende in und um Augsburg voranbringen. Startet eigene Aktionen in euren Straßen und Wohnvierteln. Wir helfen bei der Gründung weiterer Initiativen in Stadtteilen oder den Orten rund um Augsburg. Gerne kommen wir auch zu euch für Vorträge oder einen Austausch über die Vorschläge.
**Dann schreib eine Mail an [verkehrswende-augsburg@riseup.net](mailto:verkehrswende-augsburg@riseup.net).**

**Komm zu den nächsten Aktionen und spreche uns an!**


## Mach mit bei Aktionen: Regelmäßige Termine:

**Jeden letzten Freitag im Monat um 18 Uhr am Rathausplatz: Critical Mass**
[https://criticalmass-augsburg.de/](https://criticalmass-augsburg.de/)
Eine Gruppe von Radfahrer\*innen fährt gemeinsam durch die Stadt – Wir sind der Verkehr!

![Grafik: Critical Mass Plakat](/assets/CM-Western.jpg)

**Radeln zur Uni - Wir fahren gemeinsam als Critical Mass jeden Tag zur Uni**
Treffpunkte, Uhrzeit und Orte finden sich auf der Homepage:
[https://www.radeln-zur-uni.de/](https://www.radeln-zur-uni.de/)

**Im Klimacamp am Fischmarkt** kannst du bei jedem Wetter, zu jeder Jahrezeit und den ganzen Tag über zeigen, dass du mit der Klima- und Verkehrspolitik in Augsburg und Deutschlandweit unzufrieden bist und deswegen vor dem Rathaus für Klimagerechtigkeit campen, denn wir campen bis ihr handelt, wir schlafen weil die Politiker\*innen die Klimakrise verschlafen.  [https://augsburg.klimacamp.eu/](https://augsburg.klimacamp.eu/)

Außerdem gibt es auch ein **Buntes Workshop- und Vortragsprogramm im Klimacamp am Fischmarkt**
[https://augsburg.klimacamp.eu/programm/](https://augsburg.klimacamp.eu/programm/)

**Fahrradselbsthilfewerkstatt Bikekitchen**
Heilig-Kreuz-Straße 30 (jeden Do. 18-20 Uhr) 
[http://www.bikekitchen-augsburg.de/](http://www.bikekitchen-augsburg.de/)



## Rückblick zur vergangenen Aktionen:

### **2.9. 10:30 Raddemo über die B17 für die Verkehrswende in Augsburg und Bundesweit**
### **Mit anschließendem Straßenfest in der Maximiliansstraße 12:30 - 17 Uhr**

Die Fahrrad-Protesttour OhneKerosinNachBayern (OKNB) macht am Samstag in Augsburg Station auf dem Weg zum Anti-IAA Mobilitätswende Protestcamp im Luipoldpark in München (/https://mobilitaetswendecamp.noblogs.org/). In Augsburg finden zwei große Aktionen zusammen mit den über Hundert Radelnden von OKNB statt:

**Treffpunkt 10:30 am Verkehrswendecamp an der Siebentischstraße/Zoo**

Abfahrt am OKNB Verkehrswendecamp am Zoo um 11:00 Uhr

**Dazustoßen könnt ihr an folgenden Punkten:**

Seid am besten zur genannten Uhrzeit da, je nach  Geschwindigkeit kommt der Demozug ein paar Minuten später, aber so werdet ihr ihn sicher nicht verpassen ;)

11:10 an der Haltestelle Schertlinstraße 
-> Alter Postweg
11:15 Haltestelle Bukowina-Institut PCI (für eine zügige Umsetzung von Schnellbuslinien, bis die Tram gebaut ist - hier wäre Platz für ein Schnellbus-Drehkreuz) 

11:20 Fußgängerbrücke am Messezentrum (Für den Bau einer Tramlinie von Göggingen Richtig Messe, Universität und Haunstetten)
-> Anschließend Auffahrt auf die B17 

11:30 Abfahrt von der B17 an der Gabelsberger Straße 

11:35 Schießstättenstraße (neue Fahrradstraße)

11:40 Gögginger Brücke (für die Umwandlung von 2 Autospuren in Fahrradwege)

11:45 Theodor-Heuss-Platz
-> Konrad-Adenauer-Allee (Autos raus!)
-> Hallstraße (für eine sichere und autofreie Hallstraße!)

**Ca 12/12.30 je nach Tempo -> autofreie Maximiliansstraße**

Für eine dauerhafte autofreie Maxstraße inkl. Hallstraße statt Verkehrsversuch!

In der autofreien Maximiliansstraße (die Maximiliansstraße machen wir zwischen dem Moritzplatz und inklusive des Herkulesbrunnens autofrei) 
Hier haben wir auf den ehemaligen Parkflächen und auf der Straße genug Raum um unsere Forderungen und Visionen zu präsentieren und in den Austausch zu gehen. 

Die Buslinien werden vsl weiter durch die Maximiliansstraße verkehren, fordern wir schließlich auch einen Umbau/Sanierung der Maximiliansstraße mit einer festen Tramlinie und viel Raum für Leben, Natur und Kultur. Praktischerweise muss der Straßenraum weiter verengt werden und dass geht nur wenn gleichzeitig das geschnittene, Radfreundliche Pflaster auch in der Mitte  verlegt wird, damit die Randbereiche alternativen Nutzungen zugeführt werden können, andere Städte zeigen wie es gehen kann! 

Daher werden auch die ParentsForFuture, AugsburgFürAlle, Greenpeace Augsburg und viele weitere Gruppen vor Ort sein, um eine vielfältige Diskursverschiebung zu erzeugen. Wir brauchen keine Debatte über Verkehrsversuche und Geldmangel, wir brauchen endgültige Umsetzungen und Finanzierung der Verkehrswende anstatt die Subvention einer Automesse durch den Freistaat! 
Deswegen ist auch OKNB auf dem Weg zur IAA am Samstag in Augsburg und zeigt, was wirklich nötig ist. 

Wir sehen uns am Samstag auf der Raddemo und danach in der Maximiliansstraße!

### IAA Proteste in München von 5.-10. September
weitere Informationen unter https://mobilitaetswendecamp.noblogs.org/
Wir sehen uns auf dem Camp in München, dort ist viel programm und Austasch geboten, zudem gibt es die Möglichkeit, an Aktionen teilzunehmen.


**17.4: ab 18 Uhr Bürgerversammlung Göggingen, Roncallihaus, Klausenberg 7.**
18 Uhr bis 19 Uhr: Zeit für Ihr Anliegen im persönlichen Gespräch
19 Uhr bis ca. 20:30 Uhr: Im Plenum wird über die großen Themen in den Stadtvierteln gesprochen und über Ihre Anträge abgestimmt.
Weitere Infos:
[https://www.augsburg.de/oeffentlichkeitsbeteiligung/augsburger-buergerversammlung](https://www.augsburg.de/oeffentlichkeitsbeteiligung/augsburger-buergerversammlung)
Kommt zahlreich und zeigt dass wir nicht locker lassen und auch in den Stadtvierteln Klimaschutz und Verkehrswende nötig ist

**18.4 Dienstag Gehzeuge bauen auf einer Demo**
16-19 Uhr auf den Parkplätzen in der Rosenaustraße am Wittelsbacher Park gegenüber der ehemalige Rosenaux WG. Klimaschutz und Verkehrswende jetzt: Flächenverbrauch von Autos symbolisieren: Für die Umwandlung der Parkplätze in Grünflächen und sichere, breite Radwege und für mehr öffentlichen Raum für Erholung und Freizeit statt für Autos.
Kommt gerne vorbei, wir bauen viele Gehzeuge für Augsburg und symbolisieren den Flächenverbrauch von Autos und genießen die autofreien Parkplätze
Pressemitteilung dazu hier nachlesbar: [https://pad.fridaysforfuture.is/p/r.224b83120d49135a82853fccf66e85b8](https://pad.fridaysforfuture.is/p/r.224b83120d49135a82853fccf66e85b8)

**19.4 Mittwoch Bürgerversammlung in Inningen**
Inningen/Bergheim, Pfarrsaal Inningen, Bobinger Str. 59
18 Uhr bis 19 Uhr: Zeit für Ihr Anliegen im persönlichen Gespräch
19 Uhr bis ca. 20:30 Uhr: Im Plenum wird über die großen Themen in den Stadtvierteln gesprochen und über Ihre Anträge abgestimmt.
Weitere Infos:
[https://www.augsburg.de/oeffentlichkeitsbeteiligung/augsburger-buergerversammlung](https://www.augsburg.de/oeffentlichkeitsbeteiligung/augsburger-buergerversammlung)
Kommt zahlreich und zeigt dass wir nicht locker lassen und auch in den Stadtvierteln Klimaschutz und Verkehrswende nötig ist

**22.4 Samstag Verkehrswende Aktionstag am Roten Tor: Buntes Straßenfest mit Pop-Up-Radwegen**
 14:30 - 18 Uhr 
Den Straßenraum zurückerobern und umverteilen!
Dabei werden wir die autofreie Kreuzung einen Aktionsplatz verwandeln und Pop-Up Radwege auf der Straße errichten und damit zeigen, dass der Raum auch für lebendigen Austausch und Aktionen genutzt werden kann.
Geplant sind unter anderem Info- und Aktionsstände zur Verkehrswende, Programm mit Reden, künstlerischen Beiträgen zum Thema, eine Podiumsdiskussion, Informationen und Ausstellungen zur Verkehrswende und unserem Verkehrswendeplan, in dem wir konkrete Umsetzungsideen für Augsburg aufzeigen und auch weiter Sammeln, was sich in Augsurg verändern muss.
Wir wollen zeigen, dass der Straßenraum auch den Menschen und nicht den Autos gehört und eine Umverteilung des Verkehrsraums im Sinne einer sichere und zukunftsorientierte Verkehrs- und Klimapolitik nötig ist.
Mit dem Aktionstag versuchen wir, an einem Tag für einige Stunden die Situation herzustellen, die in unserem Verkehrswendeplan (verkehrswende-augsburg.de) gefordert wird. Dafür nutzen wir das Versammlungsrecht und füllen den Freiraum mit kreativen Ideen um die Verkehrswende voranzubringen.
Wir wollen eine sichere und zukunftsorientierte Verkehrs- und Klimapolitik rund um das Rote Tor und in ganz Augsburg. Für mehr lebenswerten und sicheren Raum zum Bewegen, Verweilen, Erholen und Kultur in Augsburg, auch speziell für Kinder und Jugendliche und auf Schulwegen.
PM findet sich hier: [https://etherpad.wikimedia.org/p/r.bf321b3e2210679cbd73812d45d19b17](https://etherpad.wikimedia.org/p/r.bf321b3e2210679cbd73812d45d19b17)

**23.4 Sonntag Sternfahrt nach München vom Radentschied Bayern**     
Rund um München findet eine zentrale große Sternfahrt aus dem ganzen Umland statt. Insgesamt fahren 16 polizeibegleitete      Demozüge zur zentralen Kundgebung auf den Münchner Königsplatz.      Dort gibt es ein Bühnenprogramm mit prominenten Gästen und Bands.      
Die Augsburger Demoroute eröffnet den Aktionstag. Wir starten am 23. April um 7.45 in Augsburg auf der Konrad-Adenauer-Allee und      radeln auf unserer "Demoroute A1" 77 km in gemütlichem Tempo nach      München. Unterwegs stoßen immer wieder Menschen dazu z.B. 8.35        in   Kissing, 8.55 in Mering und 9.10 in Merching.       Und natürlich ist es auch möglich, nicht bis München mit zu      radeln, sondern vorher umzudrehen.
Schmückt eure Räder, bringt sicher befestige Demoschilder und      Fahnen mit, zieht euer schönstes Fahrrad-T-Shirt an, am besten      alles in Radentscheid-blau. 
 Alle Infos zur Route gibt es immer aktuell hier: [https://muenchen.adfc.de/sternfahrt](https://muenchen.adfc.de/sternfahrt)     
Die Veranstaltung endet in München. Die Rückfahrt muss privat      organisiert werden. Unsere Erfahrung von der letzten Sternfahrt      hat jedoch gezeigt, dass die Rückreise mit dem Zug nach Augsburg      gut möglich ist. Da alle unterschiedlich lang in München      verweilen, haben sich die Fahrräder gut in den Zügen verteilt. Es      haben sich aber auch einige Leute gefunden, die zusammen zurück      geradelt sind.

**24.4 Montag 17 Uhr Bewegte Sitzung mit dem Fachforum Verkehr**
Startpunkt an der Skulptur am Staatstheater, Route über Fröhnlichstraße und Rosenaustraße und Herrmannstraße zum Kö. Offener Austausch über Verbesserungspotential und Verkehrsthematiken vor Ort.

**26.4 Mittwoch: Radeln zur Uni startet im April wieder**
Wir radeln zur Uni!
Wir wollen gemeinsam  an die Uni Radeln, um Aufmerksamkeit fürs Radfahren zu schaffen, gemeinsam Rad zufahren, uns zu vernetzen oder einfach nur Spaß dabei zu haben.
Alle Studierenden, Dozierenden, Mitarbeiter*innen und sonstige Menschen sind herzlich eingeladen, mitzufahren.
Wenn wir 16 Radfahrende oder mehr sind, dürfen wir nach § 27 StVO sogar auf der Straße fahren!
Termine und Treffpunkte findet ihr immer aktuell auf der Website
[https://www.radeln-zur-uni.de/](https://www.radeln-zur-uni.de/)
Eine Anmeldung ist nicht nötig! Schau einfach vorbei. Viel Spaß beim Radeln!



![Grafik: A8 Demo Sharepick](/assets/a8demo.jpg)

### **Fahrraddemo über die Autobahn A8**
**Am Sonntag, 5. März, für eine klimagerechte Mobilitätswende mit dem Rad auf die Autobahn A8!**

## UPDATE:
Das Gericht folgte leider der Darstellung der Stadt, dass die 2 km kurze Raddemo zu einer mehr als sechsstündigen Vollsperrung der A8 führe, und befand diese lange Zeit für unverhältnismäßig. Bei früheren Fahrraddemos auf Autobahnen, wie sie in anderen Regionen Deutschlands immer wieder vorkommen, wird die Autobahn nur für die Demo selbst gesperrt und direkt im Anschluss wieder freigegeben. Dass dieses Vorgehen darstellbar ist, hielten wir für so offensichtlich, dass wir das in unserem Eilantrag leider nicht näher begründeten.

**Wichtig! Wir treffen uns trotzdem am Sonntag um 14h am Rathausplatz für die Demo für ein Tempolimit und gegen den Autobahnausbau, für Investitionen in Schiene und ÖPNV, wird werden die A8 auch einmal überqueren und einmal unterqueren und wollen euch die Möglichkeit geben sich auf der Raddemo mit gleichgesinnten auszutauschen :)**
**Die Sternfahrten finden wie geplant statt**


**Warum?**
- für ein generelles Tempolimit und einen Ausbaustopp des Autobahnnetzs
- für ein Moratorium für den Bundesverkehrswegeplan und stattdessen Investitionen in das Schienennetz und den ÖPNV
- _gegen die Pläne der Bundesregierung, den Ausbau von über 140 Autobahnprojekten zu beschleunigen. Konkret sollen 144 Autobahnen in einer Gesamtlänge von über 1.300 Kilometern ohne jegliche Umweltverträglichkeitsprüfungen auf bis zu 10 Spuren ausgebaut werden. Laut BUND Naturschutz bedeutet dies eine zusätzliche CO2-Belastung von über 400.000 Tonnen pro Jahr und eine direkte Bedrohung von 80 Naturschutzgebieten._
- ein Tempolimit würde zu einer deutlichen und schnell wirkenden Reduktion des CO2-Ausstoßes führen und die Unfall- und Staugefahr drastisch mindern.

 **Überzeugt? - wir sehen uns mit all deinen Freund*innen am Sonntag um 14Uhr am Rathausplatz und auf der A8!**

Am 5.3. um 14 Uhr wird zum ersten Mal in Augsburg eine Fahrraddemo über die A8 stattfinden! Nachdem ein generelles Tempolimit politisch noch in weiter Ferne zu liegen scheint und die Ampel-Regierung sogar Autobahnen beschleunigt ausbauen will, zeigen wir, dass unsere Ampel für fossile Technologien und Nichthandeln 🚦auf Rot 🔴 steht!
Fahr auch du mit uns auf die A8 und zeig, dass wir auch 2023 nicht locker lassen werden, denn die Klimakrise lässt auch nicht locker!

Mach dir gerne ein verkehrssicher befestigtes Demoschild und bring deine Freund*innen mit zu dieser wichtigen Demo!
Treffpunkt ist am Sonntag, 5.3., 14 Uhr am Rathausplatz!

**Route:**
14:00 Start am Augsburger Rathausplatz. Von dort durch die Innenstadt zur MAN-Brücke. Weiter über die B2 bis zur A8-Auffahrt 73 (Augsburg-Ost).  Dann für 2,2 km über die A8 bis zur Abfahrt 74a (Friedberg). Von dort über Amagasaki-Allee, Provinostr. und Maximilianstraße zurück zum Rathausplatz. Die Route ist ca. 16km lang.
**Update: Route führt zur A8, über die B2 Brücke über die A8 und dann weiter parallel zur A8 und anschleißend durch eine A8-Unterführung, also trotzdem in Sichtweite der A8**

### **NEU: Zubringer-Sternfahrten zum Rathausplatz aus Haunstetten und Neusäß**
Übersicht über alle treffpunkte, an denen man zur Gruppe dazustoßen kann:

**Start in Haunstetten Süd um 13:00 an der Tramhaltestelle Brahmsstraße**

_Route entlang der Tramlinie 3_

13:10 Inninger Straße neben Tramhaltestelle

13:30 Europaplatz Univiertel

13:40 Schertlinstr. neben Platz an der Haltestelle

13:45 Rotes Tor vor der Freilichtbühne

13:50 Königsplatz


**Start in Neusäß um 13:10 Uhr am Volksfestplatz**

_Route: Holzweg - Oberer Schleisweg - Kobelweg_

13:40 Oberhausen Bahnhof

_über Bärenwirt_

14:10 Uhr Ankunft Herwartstr., dort sind mindestens 15 Minuten Pause geplant, dort schließen wir uns dem Zug vom Rathausplatz kommend in Richtung A8 an

_Meldet euch gerne unter verkehrswende-augsburg@riseup.net wenn ihr einen weiteren Zubringer zum Rathausplatz organisieren wollen (z.B. aus Friedberg/Hochfeld, Inningen/Göggingen oder gar aus Richtung Diedorf/Gessertshausen)_

Aktuelle Informationen und eine Zeitplan auf [verkehrswende-augsburg.de/termine](verkehrswende-augsburg.de/termine) 

### **30.9 Jeden letzten Freitag im Monat um 18 Uhr am Rathausplatz: Critical Mass**
[https://criticalmass-augsburg.de/](https://criticalmass-augsburg.de/)
Eine Gruppe von Radfahrer\*innen fährt gemeinsam durch die Stadt – Wir sind der Verkehr!

Außerdem findet von 16. - 25.9 in Augsburg die **Radlwoche**, veranstaltet von der Stadt Augsburg, statt. Informationen zu den Veranstaltungen findet ihr unter [https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/radlwoche/](https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/radlwoche/)

## **Fahrrad September**
(Genaue Informationen weiter unten)
![Grafik: Fahrrad September Plakat](/assets/plakat_fahrradseptember.jpg)
### **16.9 Auto-Frei-Tag & Parking-Day**
Der Parking Day ist ein jährlich begangener internationaler Aktionstag. Er ist der (Rück)Umwandlung von Parkplätzen zu Grünflächen, Aufenthaltsbereichen, Fahrradstellplätzen und allgemein öffentlich nutzbarem Raum gewidmet.

**07:00 - 08:00 Pop-Up-Fahrradstraße vor dem Schulzentrum in Neusäß**

Sichereres Fahradfahren und sicherer Schulweg vor dem Neusäßer Schulzentrum: Bahnunterführung als autofreie Fahrradstraße und Rembold- und Landrat-Dr. Frey-Str. als autofreie Fahrradstraße zwischen Siegfriedstr. und Pallerstr. - Platz für Menschen und sichere, gesunde Mobilität statt für Autos, für ein zukunftsorientiertes Verkehrskonzept rund um das Schulzentrum 

**09:30 - 12:00 Aktion für eine Autofreie Hallstraße**

Demonstration während den Pausen des Holbein Gymnasiums und der Ulrichschule.
Platz für Menschen statt für Autos, sichere Schulwege statt Autoposer und Autoraser, sicherer Aufenthaltsraum für Schüler*innen statt Parkplätze und Autofahren.

**15:00 - 18:00 Bahnhofstraße Autofrei** 

(zeitgleich zur [Veranstaltung zur autoarmen Innenstadt der Stadt Augsburg zum Mobilitätsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171) am Königsplatz)
Wir zeigen wie wir die Innenstadt überall nutzen wollen und erleben möchten:
Platz für Menschen statt für Autos, Aufenthaltsqualität und Lebensraum und Platz für Kommunikation, Kultur und Regeneration von Menschen auf der Straße statt Autofahren,  unerträglichen Lärm, Feinstaubbelastung und Gestank: Raum für alternatives, umwelt- und klimafreundliches Leben und Verkehr: Für einen Verkehrsversuch in der Bahnhofstraße

Wie auch bereits beim Straßenfest auf der Karlstraße seid Ihr dazu eingeladen, alternative Nutzungsmöglichkeiten der Straßen- und Parkfläche in der Bahnhofstr. und Hllstraße auszuprobieren.

### **21.9 B17-Raddemo**
**14:00 Am Messezentrum: AVV und ÖPNV drastisch ausbauen**

**neue Infos dazu: Auffahrt auf die B17 wir erst an der Bürgermeister-Ackermann-Straße sein**, d.h. wer möchte kann erst dort dazustoßten oder schon auf der Strecke davor über die Gögginger Straße und Rosenaustraße. **Abfahrt an der Messe wird ca 14:20 sein**, d.h. **ab 14:30 kann in der Rosenaustr. auf uns gewartet werden**, **ab 14:45 an der Auffahrt zur B17 an der Bürgermeister-Ackermann-Straße (B300)**. Wir freuen uns über rege Teilnahme, wir werden erstmalig auch das nördliche Stück der B17 im Stadtgebiet auf voller Länge bis zur Stuttgarter Straße befahren und dort unsere Meinung für den ÖPNV-Ausbau, den wir so dringend brauchen, kund zu geben.

Route (ca. 11km): Am Messezentrum, Friedrich-Ebert-Straße, Allgäuer Straße, Gögginger Straße, Rosenaustraße, Bgm.-Ackermann-Straße, Bundesstraße B17, Stuttgarter Straße, Biberbachstraße, zum Oberhausen P&R zur [Veranstaltung der Stadt Augsburg zur Pendlertag mit Bus und Bahn  zum Mobilitätsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171)
AVV und ÖPNV drastisch ausbauen: Vorrang-Schnellbuslinien auf bei Stau freizuhaltenden Busspuren auf der B17 von der Uni über Bhf Messe nach Oberhausen Nord P&R: klimafreundliche Mobilität für Alle statt Lärm, Stau und Stress. 

**16:00 Oberhausen Nord P&R: (Fahrrad-) Demonstration für den Tramausbau zum Bhf Gersthofen**
Start von der [Veranstaltung der Stadt Augsburg zur Pendlertag mit Bus und Bahn  zum Mobilitätsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171) aus nach Gersthofen. 
AVV und ÖPNV drastisch ausbauen: Straßenbahn zum Bhf Gersthofen verlängern: klimafreundliche Mobilität für Alle statt Lärm, Stau und Stress. 

### **22.9 Attraktiver ÖPNV**
**12:00 Göggingen Wochenmarkt: Fahrraddemonstration für gute Verbindungen und sichere Radwege**

entlang von geforderten neuen ÖPNV Verbindungen und Tramtrassen nach Hochzoll zum Zwölf Apostel Platz (ca 11km)
**Verkehrswende Jetzt - für einen atttraktiven und gut ausgebauten ÖPNV und sicherer attraktive Fahrradinfrastruktur**: Durchgängige Tramverbindung in alle Stadtteile und Ringverbindungen zwischen den Stadtteilen, besonders die **Verlängerung nach Hochzoll Süd** vom Neuen Ostfriedhof, die **Ringanbindng von Haunstetten/Univiertel über Bhf Messe und Göggingen und Rosenaustr. zum neuen HBF** und weiter über Staatstheater und Ost-West-Achse zum Jakobertor

Wir fahren von der [Veranstaltung der Stadt Augsburg am Gögginger Wochenmarkt](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171) zur Veranstaltung am 12-Apostel-Platz in Hochzoll und zeigen, woran es für die Verkehrswende in Augsburg fehlt.
zur [Veranstaltung der Stadt Augsburg zur Mobilität im Viertel zum Mobilitätsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171)


### **23.9 16 Uhr Globaler Klimastreik am Königsplatz**
Am 23. September findet der nächste globale Klimastreik statt. In Augsburg treffen wir uns um 16:00 Uhr am Königsplatz, um für Klimagerechtigkeit zu demonstrieren. Die Pläne der Ampel waren schon zu Beginn der Regierungszeit nicht ausreichend, um die 1,5 Grad Grenze einzuhalten. Die neuesten Pläne schaffen es sogar, selbst die unambitioniertesten Vorhaben des Koalitionsvertrages zu unterbieten. Dabei richten Überflutungen und Stürme auch in Deutschland schon jetzt massive Schäden an und kosten Menschenleben. Keinen Klimaschutz können wir uns nicht leisten!
Wenn selbst offensichtliche Folgen der Klimakrise, die Regierung nicht zum Handeln bringen, braucht es den Druck von der Straße! Es braucht dich! Komm mit uns am 23. September zum Königsplatz und fordere von der Regierung konsequenten Klimaschutz ein!
[https://www.fff-augsburg.de/aktuelles/](https://www.fff-augsburg.de/aktuelles/)

### **25.9 14:30 am Königsplatz: Kidical Mass**
Platz Da für die nächste Generation heißt es am 25. September, wenn die Kidical Mass wieder die Straßen erobert. Sei dabei: Ob mit Fahrrad, Laufrad oder Roller. Unsere “kinderleichte” Route ist auch für die Kleinsten geeignet.
Stellt euch mal vor: Alle fahren mit dem Rad. Mitten auf den großen Straßen Augsburgs. Dort, wo sonst nur Autos fahren dürfen. Und haben dabei viel Spaß. Familien, Kinder, Jung und Alt, Klein und Groß, Fahrräder, Laufräder, Räder mit Anhänger,... Nur Fantasie? Für einen Nachmittag wird das auch dieses Jahr wieder Realität: Bei der 4. Augsburger Kidical Mass!

Ende: Wittelsbacher Park. Hier haben wir viel Platz zum Toben und Spielen und können den Nachmittag gemeinsam ausklingen lassen und uns in einem Fahrradparcours ausprobieren. PS: Vergesst eure Klingeln nicht :
[Weitere Infrmationen beim ADFC Augsburg](https://augsburg.adfc.de/neuigkeit/die-kidical-mass-erobert-am-25-september-wieder-augsburgs-strassen)


### Veranstaltungen während dem Stadtradeln im Juli 2022
Während dem Stadtradelns🚲 wollen wir auf der Straße zeigen, dass wir die Verkehrswende brauchen.
Wir fordern echte Veränderungen statt bloßes Stadtradeln
Umsetzung von Sofortmaßnahmen für den Klimaschutz:  echte Fahrradstraßen🚲, ÖPNV🚊 ausbauen: klimafreundliche Mobilität für Alle statt Lärm, Stau und Stress und ein Tempolimit auf allen deutschen Autobahnen 

**Mittwoch 13.Juli um 20:00 Raddemo über die B17**
Route über eine geforderte Fahrradstraße durch Pfersee, über den Verlauf einer möglichen Tramlinie Richtung Leitershofen, über die B17 zur B300, dort entlang des geforderten verlaufes der geplanten Tramlinie 5 und der geforderten Tramlinie durch die Karlstraße zum Rhp.

**Freitag 15.Juli um 16:00  Verkehrswendeplan Workshop:**
Was ist der Verkehrswendeplan, welche Ideen für Augsburg fehlen noch und wie setzen wir den Plan um?

**Freitag 15.Juli um 18:00 Rademo durch Augsburg**
Entlang von geforderten Fahrradstraßen Richtung Oberhausen, über das Georgsviertel zum Oberen Graben und über das Rote Tor und Hallstraße zurück zum Rhp.

**Freitag 22.07 18:00 Raddemo zum Abschluss des Stadtradelns**
Entlang einer geforderten Fahrradstraße durch das Hochfeld vom Theodor-Heuss-Platz Richtung Messe/Universität, dann entlang der geforderten Tramverbindung Richtung Göggingen und weiter entlang von Fahrradstraßen und kritschen Stellen für Fahrradfahrer*innen an der Stadionstr. zur Rosenaustraße, wo die Tramverbindung HBF über Kongress und Göggingen zur Uni erneut angesprochen werden soll und über die Hermannstr und Karlstr. zurückzum Rhp.

**24. Juni 2022: 09:20 - 12:00 Auto-Frei-Tag in der Hallstraße**
Am Freitag von 09:20 bis 12 Uhr (während der Pausen von Holbein Gymnasium und der Ulrichschule)  machen wir die Hallstraße in den Pausen per Versammlungsrecht autofrei und demonstrieren wie eine bessere Welt aussehen kann, Fahrradstraße und Pausenraum statt Parkplätze und Autostraße.

### Mai & Juni 2022:
**03. Juni 2022: 09:20 - 12:00 1. Auto-Frei-Tag in der Hallstraße**
Für die Zeitdauer der beiden Pause des Holbein-Gymnasiums (09:30-09:50 und 11:20- 11:40) und der Ulrichschule (10-11 Uhr) wird die Hallstraße durch die Versammlungsfreiheit für den Autoverkehr gesperrt, für Aufenthaltsraum statt Parkplätze.

**29. Mai 2022: 14-17 Uhr Aktionstag auf der Karlstraße**
Eine Utopie für die Ost-West-Achse: Platz für Menschen statt für Autos, Lärm, Feinstaubbelastung und Gestank, Raum für alternatives, umwelt- und klimafreundliches Leben und Verkehr: Tramlinie und Platz für Natur und Leben statt vierpuriger Asphaltwüste.

**27. Mai 2022: 09:20 - 12:00 1. Auto-Frei-Tag in der Hallstraße**
Für die Zeitdauer der beiden Pause des Holbein-Gymnasiums (09:30-09:50 und 11:20- 11:40) und der Ulrichschule (10-11 Uhr) wird die Hallstraße durch die Versammlungsfreiheit für den Autoverkehr gesperrt.

**17.05.2022 Temporäre Autofreie Zone in der Mittagszeit vor dem St. Stefan Gymnasium**

**17.05.2022 Demo in der Hallstraße während der Pausen des Holbein Gymnasiums für eine autofreie Hallstraße**
Reaktion auf drei Unfälle in der vorgegangenen Tagen in dieser Straße
Platz für Menschen statt für Autos, sichere autofreie Fahrradstraße statt Autoposer und Autoraser, sicherer Aufenthaltsraum für Schüler*innen statt Parkplätze und Autofahren.

**13.05.2022: Demo in der Hallstraße während der Pausen des Holbein Gymnasiums für eine autofreie Hallstraße**
Platz für Menschen statt für Autos, sichere autofreie Fahrradstraße statt Autoposer und Autoraser, sicherer Aufenthaltsraum für Schüler*innen statt Parkplätze und Autofahren.

**06.05.2022 Rundumgrün Demonstration an der Kreuzung Karlstraße/Karolinenstraße**
