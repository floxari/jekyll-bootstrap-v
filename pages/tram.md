---
layout: page
title: Tram
permalink: /tram/
nav_order: 50
---

## Tram
Straßenbahn sind die leistungsfähigsten Mittel des ÖPNV. Sie brauchen trotzdem wenig und vor allem kaum versiegelte Fläche, sind barrierearm und bereits jetzt elektrisch unterwegs. Anders als Autos und Busse kommen sie auch gut in Fußgänger\*innenzonen klar. Daher soll das Hauptnetz des ÖPNV in Augsburg mit Straßenbahnen weiter ausgebaut werden.

Ausgehend von den Tramhaltestellen fahren Busse in alle weiteren Stadtteile und Orte der Umgebung - und zwar in einem dichten Takt, mit zeitlich abgestimmten und barrierenfreien und überdachten Übergängen zur Tram. 
![Symbolbild: Tram](/assets/tram_symbolbild.webp)
Foto stammt aus der [AZ vom 03.08.2020](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Bus-und-Bahn-in-Augsburg-Pro-Bahn-kritisiert-lange-Wartezeiten-id57862121.html)

<iframe width="100%" height="600px" frameborder="0" allowfullscreen src="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=99453%2C99560%2C99346"></iframe><p><a href="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845">Vollbildanzeige</a></p>

## Optimierungen des heutigen ÖPNV-Netzes in Augsburg
Folgende Verlängerungen der Tramlinien sind für die bessere Abdeckung des Ballungsraumes notwendig:

- Verlängerung der Linie 1 nach Hochzoll Süd über Hochzoll Bhf, 12-Apostel-Platz und durch den dafür bereits seit jahrzenten freigehaltenen Weg durch den Park nach Süden. Hochzoll Süd/Kuhsee soll wieder umstiegsfrei erreichbar werden!

- Verlängerung der Linie 1  nach Norden zur Hammerschmiede und Firnhaberau, um eine umstiegsfreie Verbindung in die Innenstadt zu gewährleisten zwischen die beiden Stadtteile über den P&R und dann an der Waldorfschule vorbei nach Norden. So ist eine bessere Auslastung möglich, da mehr Menschen im Einzugsgebiet wohnen und beide Stadtteile profitieren

- die schon seit jahrzenten geplante Südverlängerung der Linie 2 nach Haunstetten E-Center/Offenbach Karee (Platz für Wendeschleife schon vorhanden). Auch hier soll eine umstiegsfreie Verbindung gewährleistet werden.

- Weiterführend braucht es Querverbindungen zwischen den Ästen, um auf Störungen flexibel reagieren zu können und um Variable Linienführungen für eine gesteigerte Attraktivität des ÖPNV bieten zu können. Diese vielfältige Linienführung ist bereits im Münchner Tramnetz realität und steigert den Nutzwert enorm.

- attraktive Ringverbindungen zwischen den Tramästen, z. B.  eine Tram vom Hauptbahnhof über die Rosenau, die bestehenden Gleise der Linie 1 über den Kongress am Park nach Göggingen, auf der Allgäuer Str. geraudeaus und entlang der Friedrich-Ebert-Str. über Bhf Messe, Messezentrum, westlich am Unicamups vorbei über den Innovationspark (Forschungsallee) zur Fußball-Arena, optional eine Weiterführung über die bestehende Linie 3 nach Königsbrunn oder auf Höhe des Haunstetter Hallenbads zur Wendeschleife am Offenbach-Karee/E-Center.

- um auf Störungen flexibel reagieren zu küönnen braucht es diese entsprechende Querverbindungen, die auch im wechselseitigen 5-Min-Takt das Netzangebot diversifizieren und so die Attraktivität erhöhen.

Sinnvoll ist in dem Zuge auch eine Änderung der Trassenführung im Innenstadtbereich, um die Innenstadt breiter abzudecken.
Das bedeutet:
- Führung der Linie 6 über Moritzplatz und Maximilianstraße statt über Rotes Tor und Hochschule nach Hochzoll. Eine Haltestellenverlegung in die Mitte der Maximilianstraße könnte die Attraktivität dort nochmal erheblich steigern.
- Führung einer Linie von der Bahnhofsunterführung (Abzweigung im Tunnel schon eingeplant und vorhanden) über Staatstheater und Karlstraße zum Jakobertor. Das fordert auch das Forum Augsburg Lebenswert  [https://forum-augsburg-lebenswert.de/2019/10/27/gemeinsamer-vorschlag-fuer-die-ost-west-achse/] (https://forum-augsburg-lebenswert.de/2019/10/27/gemeinsamer-vorschlag-fuer-die-ost-west-achse/)

AZ Artikel über politische Wünsche zu Veränderung der Karlstraße: [https://www.augsburger-allgemeine.de/augsburg/Augsburg-Verkehrsberuhigung-Koalition-will-weniger-Autos-in-der-Karlstrasse-id60802301.html](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Verkehrsberuhigung-Koalition-will-weniger-Autos-in-der-Karlstrasse-id60802301.html)

Das Straßenbahnnetz der Stadt Augsburg muss schnellstmöglich erweitert werden, um die fehlenden schnellen Umstiegsmöglichkeiten vom “Stadtverkehr” auf das DB-Netz zu ermöglichen.
Folgende Verbindungen der Straßenbahnzweige an das DB-Schienennetz müssen geschaffen werden:
- Straßenbahnanbindung an die DB Strecke nach Bobingen/Buchloe/Landsberg, z.B. über eine Tramlinie aus der Rosenau über Göggingen zum Bahnhof Messe und weiter zum Innovationspark
- Verlängerung der Tram von Oberhausen Nord über das Schulzentrum zum Bahnhof Gersthofen
- Verlängerung der Straßenbahn vom Universtitäts-Klinikum an den Bahnhof in Westheim / Neusäß
- Weitere Verlängerung der Linie 3 nach Königsbrunn Süd, um auch den Süden von Königsbrunn gut an das Tramnetz anzubinden (Platz für Wendeschleife und Trassenführung schon vorhanden) [https://www.augsburger-allgemeine.de/schwabmuenchen/koenigsbrunn-start-der-linie-3-in-koenigsbrunn-was-sie-zur-strassenbahn-wissen-muessen-id61229621.html](https://www.augsburger-allgemeine.de/schwabmuenchen/koenigsbrunn-start-der-linie-3-in-koenigsbrunn-was-sie-zur-strassenbahn-wissen-muessen-id61229621.html)
- Linie 5 nach Friedberg Bhf/Innenstadt/Festplatz erweitern
- Vorschlag einer Sonderlinie zu Veranstaltungen im Gaswerk Areal (z.B. beim Modular), Schienen bis zum Gaswerk erweitern. Eine Sonderlinie neben normalem Takt ist möglich, um dortige Veranstaltungen besser anzubinden. Sonderlinien fahren schon heute bei Veranstaltungen in der MEsse oder bei Fußballspielen.
[https://www.augsburger-allgemeine.de/augsburg/Augsburg-Beim-Modular-hat-die-Tramlinie-zum-Gaswerk-ihren-Testlauf-id54485791.html](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Beim-Modular-hat-die-Tramlinie-zum-Gaswerk-ihren-Testlauf-id54485791.html)
- Weiterhin ist ein Tramlinie vom Roten Tor über MAN und Oberhausen zum Bärenkeller (aktuelle Linienführung der Buslinien 35) denkbar.
- Langfristig ist eine Direktverbindung ohne Umstiegsnotwendigkeit von Tram über Königsbrunn und Mandichosee zum Bahnhof Mering St. Afra sinnvoll, um die Attraktivität des Umstiegs und die Zuverlässigkeit der Verbindung weiter zu steigern.


## Zeitungsartikel und Politiker*innenstimmen zum Tram-Ausbau 

Gersthofen: [https://www.augsburger-allgemeine.de/augsburg-land/gersthofen-wie-gersthofen-die-radwege-ausbauen-und-sicherer-machen-will-id61488311.html](https://www.augsburger-allgemeine.de/augsburg-land/gersthofen-wie-gersthofen-die-radwege-ausbauen-und-sicherer-machen-will-id61488311.html)

Kein Reduktion des 5-Minuten-Taktes, sogar die CSU Oberbürgermeisterin Eva Weber fordert einen attraktiven Nahverkehr: [https://www.augsburger-allgemeine.de/augsburg/augsburg-5-minuten-takt-ob-pfeift-stadtwerke-zurueck-id61875506.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-5-minuten-takt-ob-pfeift-stadtwerke-zurueck-id61875506.html), [https://www.augsburger-allgemeine.de/augsburg/augsburg-5-minuten-takt-fuer-trams-soll-nicht-zurueckkehren-id61770386.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-5-minuten-takt-fuer-trams-soll-nicht-zurueckkehren-id61770386.html)

AZ Artikel zum Ausbau des Tramnetzes allgemein: [https://www.augsburger-allgemeine.de/augsburg/augsburg-die-linie-3-rollt-wo-wird-augsburgs-tramnetz-als-naechstes-ausgebaut-id61236996.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-die-linie-3-rollt-wo-wird-augsburgs-tramnetz-als-naechstes-ausgebaut-id61236996.html)


#Verlängerung der Linie 2 nach Haunstetten Süd/Edeka Center
Sogar CSU-Baureferent Merkle findet eine Verengung der Haunstetter Straße auf 2 Spuren hier sinnvoll für den Ausbau der Tram [Artikel in der AZ vom 17.11.2018](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Alte-B17-Nur-noch-zwei-statt-vier-Spuren-in-Haunstetten-id52713511.html).

## Linie 5 vom HBF zum Uniklinikum
Für eine kreuzungsfreie Überquerung der B17 schlagen wir eine Tragseilbrücke für Tram und Fahrradüberquerung vor, wie bereits in München auf der Tram Linie 23 in Schwabing über dem Mittleren Ring umgesetzt. [Wiki zur Tramlinie 23 in München](https://www.muenchenwiki.de/wiki/Trambahnlinie_23).

Weitere Informationen zur sinnvollen Trassenführung der Linie 5 und zum ÖPNV in Augsburg finden sich bei der [Arbeitsgemeinschaft Nahverkehr Augsburg](https://www.ana-ev.de/index.php?n=Main.Linie5).



## Lücken im Straßenbahnnetz am Beispiel Universitätsklinikum
Am Beispiel der Straßenbahnlinie 2 (und der geplanten Linie 5) am Universitätsklinikum Augsburg soll diese Forderung dargestellt werden.
Für Menschen, die auf kurzem Anreiseweg den innerstädtischen Knotenpunkt Königsplatz passieren, ist die heutige Straßenbahnverbindung gut geeignet. Für Bewohner des westlichen Landkreises Augsburg, z. B. aus Dinkelscherben, führt die Anreise zum Klinikum mit dem Zug über Westheim und Neusäß nach Augsburg-Oberhausen. Dort kann mit der Straßenbahnlinie 2 das Klinikum erreicht werden.

![](/assets/tram_neusass.jpg)

Zusammenfassend: Die Lücken zwischen Straßenbahnnetz und DB-Schienennetz müssen schnellstmöglich geschlossen werden, um eine schnelle Verbindung von der Stadt auf die „Landzüge“ zu ermöglichen.
