---
layout: page
title: Autofreie Zonen
permalink: /autofreiezone/
nav_order: 30
---

## Zu Fuß: Autofreie Innenstadt und Zonen, barrierefreie und breite Wege
Für eine Mobilitätswende muss der Raum neu verteilt werden, weitere Flächenversiegelung ist nicht nötig. In einer Innenstadt, in die nur Menschen, die dort wohnen, und Lieferfahrzeuge fahren können, wird viel Platz frei. Die Flächen werden umgewandelt in
Aufenthaltsorte, Fahrradstraßen und ruhige Zu-Fuß-Bereiche. Vom Durchgangsverkehr befreit werden die Innenstadt, die Ortskerne der Stadtteile sowie Bereiche rund um Kindergärten und Schulen.

Gehen ist für die meisten Menschen die einfachste Form der Bewegung. Breite Gehwege ohne Hindernisse machen sie komfortabel. Ampelphasen sollen lang genug sein, dass auch Menschen mit Mobilitätseinschränkung diese sicher überqueren können. An stark benutzten Kreuzungen braucht es durch Rundumgrün eine bequeme Möglichkeit, diese diagonal zu queren. Diese Änderungen erhöhen die
Lebensqualität der Menschen in Augsburg, insbesondere der älteren.

## Autofreie Innenstadt

Die Innenstadt von Augsburg soll, wie die Zentren aller Stadtteile und umgebenden Ortskerne auch, komplett autofrei werden. Das würde die Attraktivität erheblich erhöhen und so allen Anlieger\*innen, Geschäften, Kultureinrichtungen, Gastronomie usw. nützen. Autofreie Zonen sollte es zudem um Bildungseinrichtungen, Krankenhäuser, Altenheime usw. geben. 

Infoseite zu autofreien Innenstädten mit vielen Links zu Beispielstädten, u.a. zur ähnlich großen Stadt Pontevedra (Spanien) ([https://projektwerkstatt.de/index.php?domain_id=33&p=20951](https://projektwerkstatt.de/index.php?domain_id=33&p=20951))

Bereits jetzt gibt es in Augsburg Bereiche, die für den öffentlichen Verkehr gesperrt sind (mit Ausnahmen für Anwohner\*innen und Lieferverkehr). Der Effekt ist kaum wahrnehmbar und trotzdem sind selbst die nicht öffentlichen Straßen (mit dicken Karren) zugeparkt. Das reicht nicht! Der seit Jahren nicht umgesetzte Plan der Stadtregierung für die Befreiung der Altstadt vom Durchgangsverkehr (Durchfahrtsverbote für das Lechviertel) ist eine gute Sache. Allerdings geht auch diese Forderung nicht weit genug, weshalb wir vorschlagen dies auf den gesamten von uns aufgezeigtem Bereich auszuweiten.

### Erweiterte Fußgänger\*innenzone
Erweiterung der Fußängerzone über die gesamte Bahnhofstraße bis zum Bahnhof, wie bereits von der Stadt geplant, jedoch aufgrund von behördlichen Vorgaben und  Finanzierungsproblemen noch nicht umgesetzt (Bordsteinkanten müssten abgesenkt werden um die Straße offiziell als Fußgängerzone deklarieren zu können, dies ist teuer). Wir sagen: Eine Fußgängerzone geht auch heute schon, z. B. als verlängerte Testphase bis zum Umbau.

Eine Verlängerung der Fußgängerzone über die Barfüßerbrücke entlang der Jakoberstraße über die Fuggerei bis zur Evangelischen Kirche St. Jakob würde den Straßenzug aufwerten und das Viertel rund um die Fuggerei, eine der meistbesuchten Sehenswürdigkeiten Augsburgs, gehörig aufwerten.

Außerdem würde durch eine Sperrung der Bismarckbrücke und den Straßenzug bis zum Theodor-Heuss-Platz für den motorisierten individualverkehr eine Aufwertung des Bismarckviertels entstehen, mit viel Platz für Menschen, kulturelles Leben und Platz zum Verweilen.


### Beispiel aus der Vergangenheit:
Die Bürgermeister-Fischer-Straße: 1979 für den motorisierten Individualverkehr gesperrt und in die Fußgängerzone integriert. 
Heute ist die Bürgermeister-Fischer-Straße autofrei - und man muss lange suchen für eine Stimme, die alte Lage sei besser gewesen. So wäre es auch bei weiteren Umwandlungen von Autostraßen in Fußgänger\*innenbereiche. 

Um eine umfassende Verkehrswende voranzubringen und den Autoverkehr zurückzudrängen, sollten Teile der Stadt insgesamt für Autos nicht mehr zugänglich sein. Dabei müssen wir, so wie andere Städte es schon vorgemacht haben, in größeren Maßstäben und langfristig denken. Dabei ist die Infrastruktur für die Verkehrsmittel Fahrrad, ÖPNV und auch die Fußgänger\*innenzone zu gestalten. Durch das Wegfallen der Autos wird dann viel Platz frei, auch kleine Menschen und Tiere können sich selbstständiger und sicherer bewegen, die Luft verbessert sich, es wird jede Menge Platz für Grünflächen, soziales und kulturelles Leben auf den Straßen frei, der dann ohne ständigen Lärm und Staus zum Verweilen einlädt.

### Maxstraße autofrei - vom Bürger\*innnenantrag in den Koalitionsvertrrag bis hin zum Verkehrsversuch 
Antrag von CSU und Grünen für einen zweijährigen Verkehrsversauch der Autofreien Maxstraße, jedoch nur im Bereich zwischen Moritzplatz und Herkulesbrunnen:
https://www.augsburger-allgemeine.de/augsburg/Augsburg-Augsburg-will-moeglichst-schnell-Autos-aus-der-Maximilianstrasse-verbannen-id59736356.html

## Mehr autofreie Zonen
### Flaniermeile entlang des Äußeren Stadtgrabens
Die Straßen Vogelmaur, Obere Jakobermauer, Untere Jakobermauer, Gänsbühl und Riedlerstraße werden für den motorisierten Individdualverkehr gesperrt. Die bisherigen Straßen entlang des Äußeren Stadgrabens werden umgestaltet und das Ufer des Äußeren Stadtgrabens wird an den zugänglichen Bereichen attraktiv gestaltet. Die bisherigen Asphaltflächen und Parkflächen von Unterer Jakobermauer und Gänsmühl werden entsiegelt und mit Aufenthaltszonen, Bänken, Spielplätzen usw. attraktiv gestaltet. Die Ansiedlung von Kunst, Kultur, Straßencafés, sozialen Einrichtungen und mehr ist erwünscht. 

### Autofreie Zonen um Kindergärten und Grundschulen
Elterntaxis rollen jeden Morgen zu den Kindergärten und Grundschulen, um ihre Kids dort heil hinzubringen - würden sie doch sonst von den anderen Eltern überrollt. Dieser Wahnsinn wiederholt sich Tag für Tag. Das Gegenmittel wären autofreie Zonen um alle sensiblen Zonen. Selbst der ADAC fordert ([https://www.adac.de/verkehr/verkehrssicherheit/kindersicherheit/schulweg/elterntaxi-hol-bringzonen/](https://www.adac.de/verkehr/verkehrssicherheit/kindersicherheit/schulweg/elterntaxi-hol-bringzonen/)), dass Kinder zu Fuß oder per Fahrrad unterwegs sein sollen, um das frühzeitig zu lernen - und dass deshalb Elterntaxis, falls es sie noch gibt, mindestens 250 Meter von Kindergärten und Grundschulen wegbleiben. Das soll in Augsburg und Umgebung überall so werden! 
Es gibt eine Projektgruppe, die diese Ideen verwirklichen will: ???

- Interview "Die Straße war mal für Kinder" mit Dirk Schneidemesser, in: taz, 11.5.2021 ([https://taz.de/Verkehrsforscher-ueber-Sprache/!5766200/](https://taz.de/Verkehrsforscher-ueber-Sprache/!5766200/))


### Barrierefreiheit
Im gesamten Stadtgebiet müssen Hindernisse für Menschen mit Handicap beseitigt werden. Besonders wichtig ist die Gestaltung der Haltestellen, um einen Zustieg zu ermöglichen, und der Zugang zu öffentlichen Gebäuden, Geschäften usw. 

Bettelampeln (Drückeampeln) gegen regulär getaktete Ampeln austauschen, wo möglich, Rundumgrün zum diagonalen Queren der Kreuzung einführen.

### Autofreie Innenstadt:

![Karte](/assets/zoneflyer.png)
- Innerhalb der Straßen Oberer Graben - Rotes Tor - Schießgrabenstraße - Schätzlerstraße - Karlstraße und darüber hinaus gleiche Bedingungen wie heute in der autofreien Zone Bürgermeister-Fischer-Straße
- Linie (1) nach Lechhausen vom Hauptbahnhof über die nördliche Tunnelausfahrt und Fröhlichstr. durch die Karlstraße zum Jakobertor, Führung der Linie 2 wie bisher über Moritzplatz und Rathausplatz und der Linie 5 über die Maximilianstraße und Milchberg nach Hochzoll sowie der Linie 4 über den Fugger Boulevard zum Staatstheater
- Fahrradstraßennetz um die Innenstadt herum - soweit möglich auf viersprigen Straßen vom Autoverkehr abgetrennt in jeder Richtung auf zwei eigenen Spuren mit Rad-Verbindungen quer durch die Innenstadt und weiterführende  Achsen von dort in alle Ortsteile

### Autofreie Zonen
sofort:
- Hallstraße autofrei machen (restlichen notwendigen Autoverkehr in die Maximilianstraße im Einbahnsystem über die Parallelstraßen führen)
- Bahnhofstraße als Verkehrsversuch komplett als Fußgängerzone ausweisen, solange die Normen für Fußgänger*innenzonen durch den fehlenden Umabu nicht erfüllt werden können
- Die Altstadt für den Durchgangsverkehr sperren; dies steht auch im Koalitionsvertrag der aktuellen Stadtregierung
- Karolinenstraße und Maximilianstraße für den Durchgangsverkehr sperren, nur noch für Anlieger*innen 
- Untere Jakobermauer für den Durchgangsverkehr sperren


