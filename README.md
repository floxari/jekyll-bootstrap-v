# Jekyll Bootstrap
Template, um schnell und einfach (d.h. ohne Terminal) eine Website mit Jekyll aufzusetzen.  

[**Demo**](https://jekyll-bootstrap.vercel.app/)

## Anleitung
- Dieses Repo forken
- Neues Projekt bei [Vercel](https://vercel.com/new) erstellen und dort das zuvor geforktes Repo auswählen
- Auf "Deploy" klicken
- Fertig

## So funktionierts (technisch)

* [Jekyll](https://jekyllrb.com/) baut aus dem Quellcode die Website (Static renderer)
* [Vercel](https://github.com/vercel/vercel) führt das automatische Bauen durch (CI/CD)

## Quickstart - Redaktionelles

* Du brauchst dazu einen GitLab-Account - [hier klicken zum Registrieren](https://gitlab.com/users/sign_up).
* Dein Account braucht bestimmte Berechtigungen. Frage dafür andere Menschen, die bereits Zugriff haben 
* Um eine Seite zu bearbeiten, such sie Dir oben in der Liste der Dateien raus
* Klicke dann auf `Edit`. Du kannst die Datei dann bearbeiten
  und Deine Änderungen einreichen.
* Klicke dann unten auf *Commit changes*. Im Feld `Commit message` kannst du eine Zusammenfassung eintragen.


Je nach Einstellung arbeitest  im Hintergrund mit einer *Kopie* der Website. Du kannst dann also nichts
kaputt machen. Deine Änderungen werden vor Übernahme auf die Seite erst noch
von einem anderen Mensch durchgesehen - nicht aus autoritären Gründen sondern aus technischen. So funktioniert
Git. Alle arbeiten an ihren eigenen Kopien und eine ist eben diejenige,
die man auch im Internet sieht.

[Hier gibt es eine Anleitung für Formatierung und so](https://guides.github.com/features/mastering-markdown/).
